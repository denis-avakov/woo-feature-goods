<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-admin
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Feature_Goods_Admin {
  private $plugin_name;
  private $version;

  public function __construct($plugin_name, $version) {
    $this->plugin_name = $plugin_name;
    $this->version = $version;
  }

  public function enqueue_styles() {
    $path = plugin_dir_url(__FILE__) . 'assets/css/woo-feature-goods-admin.css';
    wp_enqueue_style($this->plugin_name, $path, array(), $this->version, 'all');
  }

  public function enqueue_scripts() {
    $path = plugin_dir_url(__FILE__) . 'assets/js/woo-feature-goods-admin.js';
    wp_enqueue_script($this->plugin_name, $path, array('jquery'), $this->version, false);

    $path_lodash = 'https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js';
    wp_enqueue_script($this->plugin_name . '-vendor-lodash', $path_lodash, array(), $this->version, false);

    $path_tinyColorPicker = 'https://cdnjs.cloudflare.com/ajax/libs/tinyColorPicker/1.1.1/jqColorPicker.min.js';
    wp_enqueue_script($this->plugin_name . '-vendor-spectrum', $path_tinyColorPicker, array(), $this->version, false);
  }

  public function register_settings_page() {
    require_once plugin_dir_path(dirname(__FILE__)) . 'side-admin/class-woo-settings-page.php';

    $options_page = new Woo_Feature_Goods_Settings();
    $options_page -> start();
  }
}
