<?php

/**
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-admin/templates
 * @author            Avakov Denis <den3er@gmail.com>
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
