(function () {
  'use strict';

  const _inputList = {
    categoryFeed: [
      { id: 'woo-feature-goods__catfeed-headline', attr: 'headline' },
      { id: 'woo-feature-goods__catfeed-products-ids', attr: 'products_ids' },
      { id: 'woo-feature-goods__catfeed-desc-image-id', attr: 'desc_image_id' },
      { id: 'woo-feature-goods__catfeed-desc-start-price', attr: 'desc_start_price' },
      { id: 'woo-feature-goods__catfeed-desc-text', attr: 'desc_text' },
      { id: 'woo-feature-goods__catfeed-last-slide-image-id', attr: 'last_slide_image_id' },
      { id: 'woo-feature-goods__catfeed-category-button-text', attr: 'category_button_text' },
      { id: 'woo-feature-goods__catfeed-category-button-link', attr: 'category_button_link' },
      { id: 'woo-feature-goods__catfeed-callback-button-text', attr: 'callback_button_text' },
      { id: 'woo-feature-goods__catfeed-callback-button-link', attr: 'callback_button_link' }
    ],
    goodsFilter: [
      { id: 'woo-feature-goods__filter-headline', attr: 'headline' },
      { id: 'woo-feature-goods__filter-category-ids', attr: 'category_ids' }
    ]
  };

  document.addEventListener('DOMContentLoaded', () => {
    const instance = jQuery('.titan-framework-panel-wrap');
    const selector = '#woo-catfeed-submit,#woo-filter-submit';

    instance.find(selector).on('click', function (event) {
      event.preventDefault();

      if (isTabActive(instance, 'category-feed')) {
        create_shortcode_category_feed(instance);
      }

      if (isTabActive(instance, 'goods-filter')) {
        create_shortcode_goods_filter(instance);
      }
    });
  });

  function isTabActive(instance, condition) {
    const activeTab = instance.find('.nav-tab-active').attr('href');
    const searchParams = new URLSearchParams(activeTab);
    return searchParams.get('tab') === condition;
  }

  function create_shortcode_category_feed(instance) {
    const form_options = instance.find('form').not('#tf-reset-form');
    const options_array = jQuery(form_options).serializeArray();

    const shortcode = createShortcodeString('[woo_category_feed', options_array, _inputList.categoryFeed);
    displayShortcodeResult(instance, '#woo-catfeed-result', shortcode);
  }

  function create_shortcode_goods_filter(instance) {
    const form_options = instance.find('form').not('#tf-reset-form');
    const options_array = jQuery(form_options).serializeArray();

    const category_ids = instance.find('.tf-multicheck-categories input').serializeArray();
    const category_ids_array = category_ids.map(function(item) {
      return item['value'];
    });

    options_array.push({
      name: 'woo-feature-goods__filter-category-ids',
      value: category_ids_array.join(', ')
    });

    const shortcode = createShortcodeString('[woo_goods_filter', options_array, _inputList.goodsFilter);
    displayShortcodeResult(instance, '#woo-filter-result', shortcode);
  }

  function createShortcodeString(start_string, options, inputList) {
    return _.reduce(options, (result, input, key) => {
      const input_instance = _.filter(inputList, { id: input.name });
      if (!_.isEmpty(input_instance) && input.value.length) {
        const attr_name = input_instance[0].attr;
        result += ' ' + attr_name + '="' + _.escape(input.value) + '"';
      }

      const isLastIteration = (key === options.length - 1);
      if (isLastIteration) result = result + ']';

      return result;
    }, start_string);
  }

  function displayShortcodeResult(instance, selector, shortcode_code) {
    const result = instance.find(selector);
    result.html(shortcode_code).show(200);
  }
}());
