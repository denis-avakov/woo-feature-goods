<?php

/**
 * The settings page functionality of the plugin.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-admin
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Feature_Goods_Settings {
  public function start() {
    $this->create_settings_page();
  }

  private function create_settings_page() {
    $titan = TitanFramework::getInstance('woo-feature-goods');

    $adminPanel = $titan->createAdminPanel(array(
      'id'         => 'woo-feature-goods',
      'name'       => __('Feature Goods', 'woo-feature-goods'),
      'icon'       => 'dashicons-share-alt'
    ));


    // - [Tab] Category Feed --------------------------------------------------

    $woo_category_feed = $adminPanel->createTab(array(
      'id'         => 'category-feed',
      'name'       => __('Category Feed', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'custom',
      'name'       => ' ',
      'custom'     => $this->create_tab_description('messages/category-feed-start.txt')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'heading',
      'name'       => __('Базовые настройки', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'text',
      'id'         => '_catfeed-headline',
      'name'       => __('Заголовок модуля', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'text',
      'id'         => '_catfeed-products-ids',
      'name'       => __('Идентификаторы продуктов (через запятую)', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'heading',
      'name'       => __('Описание категории <code>[опционально]</code>', 'woo-feature-goods'),
      'desc'       => $this->get_file_contents('messages/category-feed-desc.txt')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'upload',
      'id'         => '_catfeed-desc-image-id',
      'name'       => __('Миниатюра', 'woo-feature-goods'),
      'size'       => array(500, 500)
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'text',
      'id'         => '_catfeed-desc-start-price',
      'name'       => __('Стартовая цена', 'woo-feature-goods'),
      'unit'       => '&#8381;'
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'textarea',
      'id'         => '_catfeed-desc-text',
      'name'       => __('Описание блока', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'heading',
      'name'       => __('Последний слайд <code>[опционально]</code>', 'woo-feature-goods'),
      'desc'       => $this->get_file_contents('messages/category-feed-last-slide.txt')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'upload',
      'id'         => '_catfeed-last-slide-image-id',
      'name'       => __('Миниатюра', 'woo-feature-goods'),
      'size'       => array(300, 300)
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'heading',
      'name'       => __('Кнопка всех товаров <code>[опционально]</code>', 'woo-feature-goods'),
      'desc'       => $this->get_file_contents('messages/category-feed-button.txt')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'text',
      'id'         => '_catfeed-category-button-text',
      'name'       => __('Название', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'text',
      'id'         => '_catfeed-category-button-link',
      'name'       => __('Ссылка', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'heading',
      'name'       => __('Кнопка вызова <code>[опционально]</code>', 'woo-feature-goods'),
      'desc'       => $this->get_file_contents('messages/category-feed-callback-button.txt')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'text',
      'id'         => '_catfeed-callback-button-text',
      'name'       => __('Название', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'text',
      'id'         => '_catfeed-callback-button-link',
      'name'       => __('Ссылка', 'woo-feature-goods')
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'heading'
    ));

    $woo_category_feed->createOption(array(
      'type'       => 'custom',
      'name'       => ' ',
      'custom'     => $this->create_html_shortcode('woo-catfeed-submit', 'woo-catfeed-result', '[woo_category_feed]')
    ));


    // - [Tab] Goods Filter ---------------------------------------------------

    $woo_goods_filter = $adminPanel->createTab(array(
      'id'         => 'goods-filter',
      'name'       => __('Goods Filter', 'woo-feature-goods')
    ));

    $woo_goods_filter->createOption(array(
      'type'       => 'custom',
      'name'       => ' ',
      'custom'     => $this->create_tab_description('messages/goods-filter-start.txt')
    ));

    $woo_goods_filter->createOption(array(
      'type'       => 'heading',
      'name'       => __('Базовые настройки', 'woo-feature-goods')
    ));

    $woo_goods_filter->createOption(array(
      'type'       => 'text',
      'id'         => '_filter-headline',
      'name'       => __('Заголовок модуля', 'woo-feature-goods')
    ));

    $woo_goods_filter->createOption(array(
      'type'       => 'multicheck-categories',
      'id'         => '_filter-category-ids',
      'name'       => __('Категории', 'woo-feature-goods'),
      'taxonomy'   => 'product_cat',
      'hide_empty' => true,
      'show_count' => true,
      'select_all' => true
    ));

    $woo_goods_filter->createOption(array(
      'type'       => 'heading'
    ));

    $woo_goods_filter->createOption(array(
      'type'       => 'custom',
      'name'       => ' ',
      'custom'     => $this->create_html_shortcode('woo-filter-submit', 'woo-filter-result', '[woo_goods_filter]')
    ));


    // - [Tab] Filter Settings ------------------------------------------------

    $woo_filter_settings = $adminPanel->createTab(array(
      'id'         => 'filter-settings',
      'name'       => __('Filter Settings', 'woo-feature-goods')
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'custom',
      'name'       => ' ',
      'custom'     => $this->create_tab_description('messages/filter-settings-start.txt')
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'heading',
      'name'       => __('Настройки страницы выдачи', 'woo-feature-goods'),
      'desc'       => __(
        'Для полноценной работы фильтра необходимо создать отдельную страницу и добавить шорткод <code><mark>[woo_filter_result]</mark></code>.', 'woo-feature-goods'
      )
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'select-pages',
      'id'         => 'filter-settings-result-page-id',
      'name'       => __('Страница с результатом', 'woo-feature-goods')
    ));

    // $woo_filter_settings->createOption(array(
    //   'type'       => 'number',
    //   'id'         => 'filter-settings-result-max-products-per-feed',
    //   'name'       => __('Максимальное количество товаров на категорию', 'woo-feature-goods'),
    //   'default'    => '10',
    //   'min'        => '1',
    //   'max'        => '30'
    // ));

    $woo_filter_settings->createOption(array(
      'type'       => 'editor',
      'id'         => 'filter-settings-result-empty',
      'name'       => __('Сообщение пустой страницы', 'woo-feature-goods'),
      'desc'       => $this->create_tab_description('messages/filter-settings-result-empty.txt'),
      'rows'       => '5'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'editor',
      'id'         => 'filter-preloader-text',
      'name'       => __('Preloader Text', 'woo-feature-goods'),
      'rows'       => '5'
    ));

    // $woo_filter_settings->createOption(array(
    //   'type'       => 'heading',
    //   'name'       => __('Настройки фильтра', 'woo-feature-goods')
    // ));

    // $default_styles = file_get_contents(plugin_dir_path(dirname(__FILE__)) . 'side-admin/settings/default-styles/goods-filter.scss');

    // $woo_filter_settings->createOption(array(
    //   'type'       => 'code',
    //   'id'         => 'filter-settings-custom-styles',
    //   'name'       => __('Пользовательские стили', 'woo-feature-goods'),
    //   'desc'       => $this->create_tab_description('messages/filter-settings-custom-styles.txt'),
    //   'default'    => $default_styles,
    //   'lang'       => 'scss',
    //   'height'     => '270'
    // ));

    $woo_filter_settings->createOption(array(
      'type'       => 'heading',
      'name'       => __('Настройки атрибута цвет', 'woo-feature-goods')
    ));

    $woo_filter_settings->createOption( array(
      'type'       => 'number',
      'id'         => 'attr_color__lighten_force',
      'name'       => 'Осветление',
      'default'    => '15',
      'max'        => '100',
      'unit'       => '%'
    ));

    $woo_filter_settings->createOption( array(
      'type'       => 'number',
      'id'         => 'attr_color__darken_force',
      'name'       => 'Затемнение',
      'default'    => '15',
      'max'        => '100',
      'unit'       => '%'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__white',
      'name'       => 'Белый',
      'default'    => '#ffffff'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__white',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Белый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__bezhevyj',
      'name'       => 'Бежевый',
      'default'    => '#f2dea8'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__bezhevyj',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Бежевый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__seryj',
      'name'       => 'Серый',
      'default'    => '#bdc3c7'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__seryj',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Серый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__chernyj',
      'name'       => 'Черный',
      'default'    => '#212121'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__chernyj',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Черный</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__blue',
      'name'       => 'Синий',
      'default'    => '#0b409c'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__blue',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Синий</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__goluboj',
      'name'       => 'Голубой',
      'default'    => '#a9d2ff'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__goluboj',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Голубой</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__zelenyj',
      'name'       => 'Зеленый',
      'default'    => '#2aaf74'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__zelenyj',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Зеленый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__brown',
      'name'       => 'Коричневый',
      'default'    => '#9e7e44'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__brown',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Коричневый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__red',
      'name'       => 'Красный',
      'default'    => '#fd5959'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__red',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Красный</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__pink',
      'name'       => 'Розовый',
      'default'    => '#ec729c'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__pink',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Розовый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__yellow',
      'name'       => 'Желтый',
      'default'    => '#ffe869'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__yellow',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Желтый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__oranzhevyj',
      'name'       => 'Оранжевый',
      'default'    => '#ea9215'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__oranzhevyj',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Оранжевый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__fioletovyj',
      'name'       => 'Фиолетовый',
      'default'    => '#574b90'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__fioletovyj',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Фиолетовый</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__wood',
      'name'       => 'Дерево',
      'default'    => '#e6d3a7'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__wood',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Дерево</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__metallik',
      'name'       => 'Металлик',
      'default'    => '#506e86'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__metallik',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Металлик</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__serebro',
      'name'       => 'Серебро',
      'default'    => '#bcd3c2'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__serebro',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Серебро</b> атрибута?',
      'default'    => "false"
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'color',
      'id'         => 'attr_color__zoloto',
      'name'       => 'Золото',
      'default'    => '#d9b650'
    ));

    $woo_filter_settings->createOption(array(
      'type'       => 'checkbox',
      'id'         => 'is_enabled__zoloto',
      'name'       => ' ',
      'desc'       => 'Включить градиент для <b>Золото</b> атрибута?',
      'default'    => "false"
    ));

    $titan->createCSS('
.woo-feature-goods {
  &.goods-filter {
    .desktop-layout {
      .filter__column--color-list {
        .control-group {

          $lighten_force: $attr_color__lighten_force * 1%;
          $darken_force: $attr_color__darken_force * 1%;

          .control--white {
            .control__indicator {
              &::before {
                background-color: $attr_color__white;
              }

              @if $is_enabled__white == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__white, $lighten_force),
                    darken($attr_color__white, $darken_force)
                  );
                }
              }
            }
          }

          .control--bezhevyj {
            .control__indicator {
              &::before {
                background-color: $attr_color__bezhevyj;
              }

              @if $is_enabled__bezhevyj == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__bezhevyj, $lighten_force),
                    darken($attr_color__bezhevyj, $darken_force)
                  );
                }
              }
            }
          }

          .control--seryj {
            .control__indicator {
              &::before {
                background-color: $attr_color__seryj;
              }

              @if $is_enabled__seryj == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__seryj, $lighten_force),
                    darken($attr_color__seryj, $darken_force)
                  );
                }
              }
            }
          }

          .control--chernyj {
            .control__indicator {
              &::before {
                background-color: $attr_color__chernyj;
              }

              @if $is_enabled__chernyj == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__chernyj, $lighten_force),
                    darken($attr_color__chernyj, $darken_force)
                  );
                }
              }
            }
          }

          .control--blue {
            .control__indicator {
              &::before {
                background-color: $attr_color__blue;
              }

              @if $is_enabled__blue == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__blue, $lighten_force),
                    darken($attr_color__blue, $darken_force)
                  );
                }
              }
            }
          }

          .control--goluboj {
            .control__indicator {
              &::before {
                background-color: $attr_color__goluboj;
              }

              @if $is_enabled__goluboj == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__goluboj, $lighten_force),
                    darken($attr_color__goluboj, $darken_force)
                  );
                }
              }
            }
          }

          .control--zelenyj {
            .control__indicator {
              &::before {
                background-color: $attr_color__zelenyj;
              }

              @if $is_enabled__zelenyj == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__zelenyj, $lighten_force),
                    darken($attr_color__zelenyj, $darken_force)
                  );
                }
              }
            }
          }

          .control--brown {
            .control__indicator {
              &::before {
                background-color: $attr_color__brown;
              }

              @if $is_enabled__brown == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__brown, $lighten_force),
                    darken($attr_color__brown, $darken_force)
                  );
                }
              }
            }
          }

          .control--red {
            .control__indicator {
              &::before {
                background-color: $attr_color__red;
                content: "";
              }

              @if $is_enabled__red == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__red, $lighten_force),
                    darken($attr_color__red, $darken_force)
                  );
                }
              }
            }
          }

          .control--pink {
            .control__indicator {
              &::before {
                background-color: $attr_color__pink;
              }

              @if $is_enabled__pink == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__pink, $lighten_force),
                    darken($attr_color__pink, $darken_force)
                  );
                }
              }
            }
          }

          .control--yellow {
            .control__indicator {
              &::before {
                background-color: $attr_color__yellow;
              }

              @if $is_enabled__pink == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__pink, $lighten_force),
                    darken($attr_color__pink, $darken_force)
                  );
                }
              }
            }
          }

          .control--oranzhevyj {
            .control__indicator {
              &::before {
                background-color: $attr_color__oranzhevyj;
              }

              @if $is_enabled__oranzhevyj == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__oranzhevyj, $lighten_force),
                    darken($attr_color__oranzhevyj, $darken_force)
                  );
                }
              }
            }
          }

          .control--fioletovyj {
            .control__indicator {
              &::before {
                background-color: $attr_color__fioletovyj;
              }

              @if $is_enabled__fioletovyj == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__fioletovyj, $lighten_force),
                    darken($attr_color__fioletovyj, $darken_force)
                  );
                }
              }
            }
          }

          .control--wood {
            .control__indicator {
              &::before {
                background-color: $attr_color__wood;
              }

              @if $is_enabled__wood == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__wood, $lighten_force),
                    darken($attr_color__wood, $darken_force)
                  );
                }
              }
            }
          }

          .control--metallik {
            .control__indicator {
              &::before {
                background-color: $attr_color__metallik;
              }

              @if $is_enabled__metallik == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__metallik, $lighten_force),
                    darken($attr_color__metallik, $darken_force)
                  );
                }
              }
            }
          }

          .control--serebro {
            .control__indicator {
              &::before {
                background-color: $attr_color__serebro;
              }

              @if $is_enabled__serebro == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__serebro, $lighten_force),
                    darken($attr_color__serebro, $darken_force)
                  );
                }
              }
            }
          }

          .control--zoloto {
            .control__indicator {
              &::before {
                background-color: $attr_color__zoloto;
              }

              @if $is_enabled__zoloto == 1 {
                &::before {
                  background-image: linear-gradient(
                    lighten($attr_color__zoloto, $lighten_force),
                    darken($attr_color__zoloto, $darken_force)
                  );
                }
              }
            }
          }

        } // end .control-group
      } // end .filter__column--color-list
    } // end .desktop-layout
  } // end &.goods-filter
} // end .woo-feature-goods
    ');

    $woo_filter_settings->createOption(array(
      'type'       => 'save',
      'save'       => __('Сохранить', 'woo-feature-goods'),
      'reset'      => __('Сбросить', 'woo-feature-goods')
    ));
  }


  // - [utilities] ------------------------------------------------------------

  private function create_tab_description($path = '') {
    return sprintf(
      '<span class="tab-description">%s</span>',
      $this->get_file_contents($path)
    );
  }

  private function get_file_contents($path = '') {
    $settings_folder = plugin_dir_path(dirname(__FILE__)) . 'side-admin/settings/';
    return file_get_contents(__($settings_folder .  $path, 'woo-feature-goods'));
  }

  private function create_html_shortcode($submit_id, $result_id, $shortcode_base) {
    $submit = '<button id="%s" class="button button-secondary">Создать шорткод</button><br><br>';
    $result = '<code id="%s" class="woo-feature-goods__shortcode" style="display: none;">%s</code>';
    return sprintf($submit . $result, $submit_id, $result_id, $shortcode_base);
  }
}
