<?php

/**
 * The plugin bootstrap file \(ಠ_ಠ)/ which includes all of the dependencies
 * used by the plugin, registers the activation and deactivation functions and
 * defines a function that starts the plugin.
 *
 * @package           Woo_Feature_Goods
 * @author            Avakov Denis <den3er@gmail.com>
 * @license           MIT License
 *
 * @wordpress-plugin
 * Plugin Name:       Woo Feature Goods
 * Plugin URI:        https://bitbucket.org/den3er/woo-feature-goods
 * Description:       Hippity hoppity get off my property.
 * Version:           1.8.1
 * Author:            Avakov Denis
 * Author URI:        https://github.com/den3er
 * License:           MIT License
 * License URI:       https://opensource.org/licenses/MIT
 * Text Domain:       woo-feature-goods
 * Domain Path:       /languages
 */

if (!defined('ABSPATH')) exit;

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woo-activator.php
 */
register_activation_hook(__FILE__, function() {
  require_once plugin_dir_path(__FILE__) . 'includes/class-woo-activator.php';
  Woo_Feature_Goods_Activator::activate();
});

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woo-deactivator.php
 */
register_deactivation_hook(__FILE__, function() {
  require_once plugin_dir_path(__FILE__) . 'includes/class-woo-deactivator.php';
  Woo_Feature_Goods_Deactivator::deactivate();
});

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-woo-plugin.php';

/**
 * Since everything within the plugin is registered via hooks, then kicking off
 * the plugin from this point in the file does not affect the page life cycle.
 */
function run_woo_feature_goods() {
  $plugin = new Woo_Feature_Goods();
  $plugin -> run();
}

run_woo_feature_goods();
