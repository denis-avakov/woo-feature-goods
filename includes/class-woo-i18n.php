<?php

/**
 * Loads and defines the internationalization files for this plugin so that it
 * is ready for translation.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/includes
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Feature_Goods_i18n {
  public function load_plugin_textdomain() {
    load_plugin_textdomain('woo-feature-goods', false, dirname(dirname(plugin_basename(__FILE__))) . '/languages/');
  }
}
