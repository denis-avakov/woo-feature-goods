<?php

/**
 * A class definition that includes attributes and functions used across both
 * the public-facing side of the site and the admin area.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/includes
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Feature_Goods {
  protected $loader;
  protected $plugin_name;
  protected $current_version;

  /**
   * Set the plugin name and the plugin version that can be used throughout the
   * plugin. Load the dependencies, define the locale and set the hooks for the
   * admin area and the public-facing side of the site.
   */
  public function __construct() {
    $this->plugin_name = 'woo-feature-goods';
    $this->current_version = '1.8.0';

    $this->load_dependencies();
    $this->set_locale();
    $this->define_admin_hooks();
    $this->define_public_hooks();
  }

  /**
   * Load the required dependencies for this plugin.
   *
   * TitanFrameworkEmbedder   - defines vendor functionality for settings page
   * Woo_Feature_Goods_Loader - orchestrates the hooks of the plugin
   * Woo_Feature_Goods_i18n   - defines internationalization functionality
   * Woo_Feature_Goods_Admin  - defines all hooks for the admin area
   * Woo_Feature_Goods_Public - defines all hooks for the public side of the site
   */
  private function load_dependencies() {
    require_once plugin_dir_path(dirname(__FILE__)) . 'vendor/titan-framework/titan-framework-embedder.php';
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-woo-loader.php';
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-woo-i18n.php';
    require_once plugin_dir_path(dirname(__FILE__)) . 'side-admin/class-woo-admin.php';
    require_once plugin_dir_path(dirname(__FILE__)) . 'side-public/class-woo-public.php';

    $this->loader = new Woo_Feature_Goods_Loader();
  }

  /**
   * Uses the Woo_Feature_Goods_i18n class in order to set the domain and to
   * register the hook with WordPress.
   */
  private function set_locale() {
    $plugin_i18n = new Woo_Feature_Goods_i18n();
    $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
  }

  /**
   * Register all of the hooks related to the admin area functionality of the
   * plugin and vendor admin settings.
   */
  private function define_admin_hooks() {
    $plugin_admin = new Woo_Feature_Goods_Admin($this->get_plugin_name(), $this->get_version());
    $this->loader->add_action('tf_create_options', $plugin_admin, 'register_settings_page');

    if ($this->is_page('plugin-settings')) {
      $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
      $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
    }
  }

  /**
   * Register all of the hooks related to the public-facing functionality of
   * the plugin.
   */
  private function define_public_hooks() {
    $plugin_public = new Woo_Feature_Goods_Public($this->get_plugin_name(), $this->get_version());

    $this->loader->add_shortcode('woo_category_feed', $plugin_public, 'register_shortcode_category_feed');
    $this->loader->add_shortcode('woo_goods_filter', $plugin_public, 'register_shortcode_goods_filter');
    $this->loader->add_shortcode('woo_filter_result', $plugin_public, 'register_shortcode_filter_result');

    $this->loader->add_action('wp_ajax_get_filter_count', $plugin_public, 'ajax_filter_goods_counter');
    $this->loader->add_action('wp_ajax_nopriv_get_filter_count', $plugin_public, 'ajax_filter_goods_counter');

    $this->loader->add_action('wp_ajax_get_filter_pagination', $plugin_public, 'ajax_filter_result_pagination');
    $this->loader->add_action('wp_ajax_nopriv_get_filter_pagination', $plugin_public, 'ajax_filter_result_pagination');
  }

  /**
   * Check if the $page_name parameter is mathed to current page query string,
   * this function also uses presets for more simple syntax.
   */
  private function is_page($page_name = '') {
    $current_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $current_query = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);

    $is_admin_page = (strpos($current_path, '/wp-admin/') !== false);
    $has_query_string = !empty($current_query);

    $is_matched = false;

    if ($is_admin_page && $has_query_string) {
      parse_str($current_query, $query_string);

      switch ($page_name) {
        case 'plugin-settings':
          $is_matched = ($query_string['page'] === $this->get_plugin_name());
          break;

        default:
          $is_matched = ($current_path === $page_name);
          break;
      }
    }

    return $is_matched;
  }

  /**
   * The name of the plugin used to uniquely identify it within the context of
   * WordPress and to define internationalization functionality.
   */
  public function get_plugin_name() {
    return $this->plugin_name;
  }

  /**
   * Retrieve the current version number of the plugin.
   */
  public function get_version() {
    return $this->current_version;
  }

  /**
   * Run the loader to execute all of the hooks with WordPress.
   */
  public function run() {
    $this->loader->run();
  }
}
