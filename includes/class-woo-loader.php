<?php

/**
 * Maintain a list of all hooks that are registered throughout the plugin and
 * register them with the WordPress API. Call the run function to execute the
 * list of actions and filters.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/includes
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Feature_Goods_Loader {
  protected $actions;
  protected $filters;
  protected $shortcodes;

  public function __construct() {
    $this->actions = array();
    $this->filters = array();
    $this->shortcodes = array();
  }

  /**
   * Add a new action to the collection to be registered with WordPress.
   *
   * @param string $hook          - the name of the action that is being registered
   * @param object $component     - a reference to the instance of the object on which the action is defined
   * @param string $callback      - the name of the function definition on the $component
   * @param int    $priority      - /optional/ the priority at which the function should be fired
   * @param int    $accepted_args - /optional/ the number of arguments that should be passed to the $callback
   */
  public function add_action($hook, $component, $callback, $priority = 10, $accepted_args = 1) {
    $this->actions = $this->add($this->actions, $hook, $component, $callback, $priority, $accepted_args);
  }

  /**
   * Add a new filter to the collection to be registered with WordPress.
   *
   * @param string $hook          - the name of the filter that is being registered
   * @param object $component     - a reference to the instance of the object on which the filter is defined
   * @param string $callback      - the name of the function definition on the $component
   * @param int    $priority      - /optional/ the priority at which the function should be fired
   * @param int    $accepted_args - /optional/ the number of arguments that should be passed to the $callback
   */
  public function add_filter($hook, $component, $callback, $priority = 10, $accepted_args = 1) {
    $this->filters = $this->add($this->filters, $hook, $component, $callback, $priority, $accepted_args);
  }

  /**
   * Add a new shortcode to the collection to be registered with WordPress.
   *
   * @param string $tag           - the name of the new shortcode
   * @param object $component     - a reference to the instance of the object on which the shortcode is defined
   * @param string $callback      - the name of the function that defines the shortcode
   * @param int    $priority      - /optional/ the priority at which the function should be fired
   * @param int    $accepted_args - /optional/ the number of arguments that should be passed to the $callback
   */
  public function add_shortcode($tag, $component, $callback, $priority = 10, $accepted_args = 1) {
    $this->shortcodes = $this->add($this->shortcodes, $tag, $component, $callback, $priority, $accepted_args);
  }

  /**
   * A utility function that is used to register the actions and hooks into a
   * single collection.
   *
   * @access private
   * @param  array  $hooks         - the collection of hooks that is being registered (actions or filters)
   * @param  string $hook          - the name of the filter that is being registered
   * @param  object $component     - a reference to the instance of the object on which the filter is defined
   * @param  string $callback      - the name of the function definition on the $component
   * @param  int    $priority      - the priority at which the function should be fired
   * @param  int    $accepted_args - the number of arguments that should be passed to the $callback
   * @return array                 - the collection of actions and filters registered
   */
  private function add($hooks, $hook, $component, $callback, $priority, $accepted_args) {
    $hooks[] = array(
      'hook'          => $hook,
      'component'     => $component,
      'callback'      => $callback,
      'priority'      => $priority,
      'accepted_args' => $accepted_args
    );

    return $hooks;
  }

  public function run() {
    foreach ($this->filters as $hook) {
      $component = array($hook['component'], $hook['callback']);
      add_filter($hook['hook'], $component, $hook['priority'], $hook['accepted_args']);
    }

    foreach ($this->actions as $hook) {
      $component = array($hook['component'], $hook['callback']);
      add_action($hook['hook'], $component, $hook['priority'], $hook['accepted_args']);
    }

    foreach ($this->shortcodes as $hook) {
      $component = array($hook['component'], $hook['callback']);
      add_shortcode($hook['hook'], $component, $hook['priority'], $hook['accepted_args']);
    }
  }
}
