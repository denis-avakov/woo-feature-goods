<?php

/**
 * This class defines all code necessary to run during the plugin's
 * deactivation.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/includes
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Feature_Goods_Deactivator {
  public static function deactivate() {

  }
}
