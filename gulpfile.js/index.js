/**
 * Rather than manage one giant configuration file responsible for creating
 * multiple tasks, each task has been broken out into its own file in './tasks'
 * folder. Any files in that directory get automatically required below.
 */

'use strict';

const gulp    = require('gulp');
const plugins = require('gulp-load-plugins')();
const config  = require('./config');

// define development environment
global.isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

gulp.task('styles', require('./tasks/styles')(gulp, plugins));

gulp.task('watch', () => {
  gulp.watch(config.styles.entry, gulp.series('styles'));
});

gulp.task('default', gulp.series('styles', gulp.parallel('watch')));
