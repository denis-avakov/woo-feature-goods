'use strict';

const path = require('path');

module.exports = (pathArr) => {
  return pathArr.map((value) => {
    return path.resolve(value);
  });
};
