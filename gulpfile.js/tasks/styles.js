'use strict';

const gulpif     = require('gulp-if');
const sourcemaps = require('gulp-sourcemaps');
const postcss    = require('gulp-postcss');
const sass       = require('gulp-sass');
const cssnano    = require('gulp-cssnano');

const paths      = require('../lib/create-paths');
const config     = require('../config');

module.exports = (gulp, plugins) => {
  return () => {
    const pluginsPostCSS = [
      require('postcss-initial'),
      require('lost'),
      require('postcss-class-patterns'),
      require('postcss-quantity-queries'),
      require('autoprefixer')(config.styles.autoprefixer)
    ];

    return gulp.src(paths(config.styles.entry))
      .pipe(gulpif(global.isDevelopment, sourcemaps.init()))
      .pipe(sass())
      .pipe(postcss(pluginsPostCSS))
      .pipe(gulpif(!global.isDevelopment, cssnano()))
      .pipe(gulpif(global.isDevelopment, sourcemaps.write()))
      .pipe(gulp.dest((file) => {
        return file.base + '../css';
      }));
  };
};
