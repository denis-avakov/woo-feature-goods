'use strict';

module.exports = {
  styles: {
    entry: [
      './side-public/assets/scss/**/*.scss',
      './side-admin/assets/scss/**/*.scss'
    ],
    autoprefixer: {
      browsers: ['last 2 versions']
    },
    reporter: {
      clearReportedMessages: true
    }
  }
};
