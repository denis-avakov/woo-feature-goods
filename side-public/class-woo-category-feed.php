<?php

/**
 * The public-facing functionality of the category feed shortcode.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-public
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Category_Feed_Shortcode {
  private $atts;

  public function __construct($atts) {
    $this->atts = $atts;
  }

  public function start() {
    $atts = shortcode_atts(array(
      'headline'             => '',
      'products_ids'         => '',
      'desc_image_id'        => '',
      'desc_start_price'     => '',
      'desc_text'            => '',
      'last_slide_image_id'  => '',
      'category_button_text' => '',
      'category_button_link' => '',
      'callback_button_text' => '',
      'callback_button_link' => '',

      'feature_image_id'     => '',
      'feature_start_price'  => '',
      'feature_desc'         => '',
      'last_unit_image_id'   => '',
      'category_link'        => '',
      'callback_link'        => ''
    ), $this->atts);

    // fallback to rewrite old atts
    if (!empty($atts['feature_image_id'])) $atts['desc_image_id'] = $atts['feature_image_id'];
    if (!empty($atts['feature_start_price'])) $atts['desc_start_price'] = $atts['feature_start_price'];
    if (!empty($atts['feature_desc'])) $atts['desc_text'] = $atts['feature_desc'];
    if (!empty($atts['last_unit_image_id'])) $atts['last_slide_image_id'] = $atts['last_unit_image_id'];
    if (!empty($atts['category_link'])) $atts['category_button_link'] = $atts['category_link'];
    if (!empty($atts['callback_link'])) $atts['callback_button_link'] = $atts['callback_link'];
    // --- cut here -----------------------------------------------------------

    $products_ids_array = $this->convert_string_to_array($atts['products_ids']);
    $category_link = (!empty($atts['category_button_link'])) ? $atts['category_button_link'] : '#';

    $category_button = $this->create_category_button(array(
      'text' => $atts['category_button_text'],
      'link' => $atts['category_button_link']
    ));

    $callback_button = $this->create_callback_button(array(
      'text' => $atts['callback_button_text'],
      'link' => $atts['callback_button_link']
    ));

    $part_first_slide = $this->create_first_slide(array(
      'desc_image_id'    => $atts['desc_image_id'],
      'desc_start_price' => $atts['desc_start_price'],
      'desc_text'        => $atts['desc_text'],
      'category_link'    => $category_link,
      'category_button'  => $category_button,
      'callback_button'  => $callback_button
    ));

    $part_last_slide = $this->create_last_slide(array(
      'last_slide_image_id' => $atts['last_slide_image_id'],
      'category_link'       => $category_link,
      'category_button'     => $category_button,
      'callback_button'     => $callback_button
    ));

    include (plugin_dir_path(__FILE__) . 'templates/category-feed/main.php');
  }

  private function convert_string_to_array($string) {
    $verified_string = preg_replace('/\.$/', '', $string);
    return explode(', ', $verified_string);
  }

  private function create_category_button($atts) {
    if (!empty($atts['link'])) {
      $class_name = 'button-normal button-primary button--category';
      $button_name = (!empty($atts['text'])) ? $atts['text'] : __('Все товары', 'woo-feature-goods');

      return sprintf(
        '<a class="%s" href="%s">%s</a>',
        $class_name, $atts['link'], $button_name
      );
    }
  }

  private function create_callback_button($atts) {
    if (!empty($atts['link'])) {
      $class_name = 'button-normal button-secondary button--callback';
      $button_name = (!empty($atts['text'])) ? $atts['text'] : __('Вызов', 'woo-feature-goods');

      return sprintf(
        '<a class="%s" href="%s">%s</a>',
        $class_name, $atts['link'], $button_name
      );
    }
  }

  private function create_first_slide($atts) {
    if (!empty($atts['desc_image_id']) && !empty($atts['desc_text'])) {
      $image_url = wp_get_attachment_url($atts['desc_image_id']);
      $cover_inline_style = sprintf('style="background-image: url(%s);"', $image_url);

      ob_start();
      include (plugin_dir_path(__FILE__) . 'templates/category-feed/part-first-slide.php');
      return ob_get_clean();
    }
  }

  private function create_last_slide($atts) {
    $has_cover_image = !empty($atts['last_slide_image_id']);
    $cover_image_class_modifier = ($has_cover_image) ? 'cover-image--has-background' : 'is-bordered-slide';

    $image_url = ($has_cover_image) ? wp_get_attachment_url($atts['last_slide_image_id']) : null;
    $cover_inline_style = sprintf('style="background-image: url(%s);"', $image_url);

    ob_start();
    include (plugin_dir_path(__FILE__) . 'templates/category-feed/part-last-slide.php');
    return ob_get_clean();
  }
}
