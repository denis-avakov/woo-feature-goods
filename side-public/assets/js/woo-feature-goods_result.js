(function () {
  'use strict';

  document.addEventListener('DOMContentLoaded', function () {
    var instanceList = document.querySelectorAll('.filter-result-section');

    if (instanceList.length !== 0) {
      for (var i = 0, len = instanceList.length; i < len; i++) {
        var instance = jQuery(instanceList[i]);
        createNewButtonListener(instance);
      }
    }
  });

  function createNewButtonListener(instance) {
    var category_id = instance.find('*[data-category_id]').data('category_id');
    var submit_button = instance.find('#show-more');

    submit_button.on('click', function (event) {
      event.preventDefault ? event.preventDefault() : (event.returnValue = false);

      var query_string = [
        '&category_list=' + category_id,
        '&opacity_list=' + getParameterByName('opacity_list'),
        '&color_list=' + getParameterByName('color_list'),
        '&exclude_product_ids=' + getProductListIds(instance).join(',')
      ];

      submit_button.addClass('spin-that-shit');

      requestPagination(query_string.join(), function (response) {
        if (response !== 'silence is golden') {
          var productList = jQuery(response).find('.type-product');
          instance.find('.shop-products').append(productList).hide().fadeIn(600);
          submit_button.removeClass('spin-that-shit');

          instance.find('[title]').attr('title', function (index, value) {
            return value.replace('<br>', '');
          });

          instance.find('.secondary_image').attr('title', 'Нажмите чтобы рассчитать по вашим размерам.');
        } else {
          submit_button.hide(200);
        }
      });
    });
  }

  function getProductListIds(instance) {
    return instance.find('*[data-product_id]').map(function () {
      return this.getAttribute('data-product_id');
    }).get();
  }

  function getParameterByName(name, url) {
    if (typeof url === 'undefined') url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');

    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
    var results = regex.exec(url);

    if (!results) return null;
    if (!results[2]) return '';

    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  var requestPagination = (function () {
    var time_window = 750; // time in ms
    var timeout;

    var send = function (query_string, callback) {
      var ajax_url = filter_result_pagination.ajax_url + "?ts=" + new Date().getMilliseconds();

      var data = {
        action: 'get_filter_pagination',
        security: filter_result_pagination.security,
        query_string: query_string
      };

      jQuery.post(ajax_url, data, callback);
    };

    return function () {
      var context = this;
      var args = arguments;
      clearTimeout(timeout);
      timeout = setTimeout(function () {
        send.apply(context, args);
      }, time_window);
    };
  }());

}());
