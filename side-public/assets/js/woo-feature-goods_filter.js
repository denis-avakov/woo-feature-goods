(function () {
  'use strict';

  document.addEventListener('DOMContentLoaded', function () {
    var instanceList = document.querySelectorAll('.woo-feature-goods.goods-filter');

    jQuery(window).bind('pageshow', function (event) {
      if (event.originalEvent.persisted) {
        window.location.reload()
      }
    });

    if (instanceList.length !== 0) {
      for (var i = 0, len = instanceList.length; i < len; i++) {
        var desk_instance = jQuery(instanceList[i]);
        var mobi_instance = createMobileInstance(desk_instance);

        toggleFilter(desk_instance, mobi_instance);
        updateResponsive(desk_instance, mobi_instance);
        setInitialSubmitLink(desk_instance, mobi_instance);
        updateFilterCounter(desk_instance, mobi_instance);
        updateFilterSubmitLink(desk_instance, mobi_instance);
        checkBrowser();
      }
    }
  });

  function checkBrowser() {
    var isMobile = false;

    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

    if (isMobile) jQuery('body').addClass('filter-is-mobile');
  }

  function createMobileInstance(instance) {
    return instance.find('.mobile-layout').appendTo('body').wrapAll('<div class="woo-feature-goods goods-filter" />');
  }

  function toggleFilter(desk_instance, mobi_instance) {
    var deskFilter = desk_instance.find('.filter');
    var headline = desk_instance.find('.headline');

    if (detectIE() === 11) {
      deskFilter.find('.products-amount').hide();
      deskFilter.find('.filter__column--category-list .control__indicator,.filter__column--opacity-list .control__indicator').hide();

      var mobi_filter = mobi_instance.find('.filter');
      mobi_filter.find('.products-amount').hide();
    }

    desk_instance.find('.headline__link,.button--close-filter')
      .on('click', function (event) {
        event.preventDefault();

        deskFilter.toggle(200);
        headline.toggleClass('headline--filter-closed');
      });

    desk_instance.find('.button--open-mobile-layout')
      .on('click', function (event) {
        event.preventDefault();
        var thos = this;
        jQuery(this.getAttribute('href')).toggle(200);
        jQuery('body').addClass('mobile-layout-is-active');
        jQuery('#wpadminbar,div.vg-cart,div.to-top').hide();
      });

    desk_instance.find('#submit')
      .on('click', function (event) {
        event.preventDefault();

        desk_instance.find('.preloader').show();

        setTimeout(function () {
          window.location.href = desk_instance.find('#submit').attr('href');
        }, 200);
      });

    mobi_instance.find('.button--close-mobile-layout')
      .on('click', function (event) {
        event.preventDefault();
        jQuery(this.getAttribute('href')).toggle(200);
        jQuery('body').removeClass('mobile-layout-is-active');
        jQuery('#wpadminbar,div.vg-cart,div.to-top').show();
      });

    mobi_instance.find('.button--mobile-reset')
      .on('click', function (event) {
        event.preventDefault();
        mobi_instance.find('input[type="checkbox"]:checked').trigger('click');
      });

    // mobi_instance.find('#submit')
    //   .on('click', function (event) {
    //     mobi_instance.find('.preloader').show();
    //   });
  }

  function updateResponsive(desk_instance, mobi_instance) {
    var mobi_column = mobi_instance.find('.filter__column');
    var mobi_submit = mobi_instance.find('.submit-section');
    var mobi_reset = mobi_instance.find('.button--mobile-reset');

    var initialHeight = getWindowHeight();

    mobi_column.css('max-height', (initialHeight + 45));
    mobi_submit.css('height', minusPercent(initialHeight, 30));
    mobi_reset.css('height', minusPercent(initialHeight, 70));

    resizeEventListener(function () {
      var newHeight = getWindowHeight();

      mobi_column.css('max-height', (newHeight + 45));
      mobi_submit.css('height', minusPercent(newHeight, 30));
      mobi_reset.css('height', minusPercent(newHeight, 70));
    });
  }

  function setInitialSubmitLink(desk_instance, mobi_instance) {
    var desk_filter = desk_instance.find('.filter');
    var mobi_filter = mobi_instance.find('.filter');

    var desk_submit = desk_filter.find('#submit');
    var mobi_submit = mobi_filter.find('#submit');

    var desk_query_string = getElementsForSerialize(desk_filter, true);
    var mobi_query_string = getElementsForSerialize(mobi_filter, true);

    desk_submit.attr('href', desk_query_string.join('?'));
    mobi_submit.attr('href', mobi_query_string.join('?'));
  }

  function updateFilterCounter(desk_instance, mobi_instance) {
    var desk_filter = desk_instance.find('.filter');
    var mobi_filter = mobi_instance.find('.filter');

    desk_filter.find('input[type="checkbox"]').on('click', function () {
      var query_string = getElementsForSerialize(desk_filter);
      requestFilter(query_string[1], desk_filter, function (response) {
        desk_filter.find('.products-amount').html(function () {
          return create_plural_string(response, ['товар выбран', 'товара выбрано', 'товаров выбрано']);
        }).removeClass('spin-that-shit');
      });
    });

    mobi_filter.find('input[type="checkbox"]').on('click', function () {
      var query_string = getElementsForSerialize(mobi_filter);
      requestFilter(query_string[1], mobi_filter, function (response) {
        mobi_filter.find('.products-amount').html(function () {
          return create_plural_string(response, ['товар', 'товара', 'товаров']);
        }).removeClass('spin-that-shit');
      });
    });
  }

  function updateFilterSubmitLink(desk_instance, mobi_instance) {
    var desk_filter = desk_instance.find('.filter');
    var mobi_filter = mobi_instance.find('.filter');

    desk_filter.find('input[type="checkbox"]').on('click', function () {
      var desk_submit = desk_filter.find('#submit');
      var query_string = getElementsForSerializeSubmit(desk_filter);
      desk_submit.attr('href', query_string.join('?'));
    });

    mobi_filter.find('input[type="checkbox"]').on('click', function () {
      var desk_submit = mobi_filter.find('#submit');
      var query_string = getElementsForSerializeSubmit(mobi_filter);
      desk_submit.attr('href', query_string.join('?'));
    });
  }


  // - Helpers ----------------------------------------------------------------

  function getWindowHeight() {
    var hasToolbar = jQuery('body').hasClass('admin-bar');
    var currentHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    return (hasToolbar) ? currentHeight - 45 : currentHeight;
  }

  function minusPercent(number, percent) {
    return number - (number * (percent / 100));
  }

  function resizeEventListener(callback) {
    var timeout = false;
    var delay = 250;

    window.addEventListener('resize', function () {
      clearTimeout(timeout);
      timeout = setTimeout(callback, delay);
    });
  }

  function getElementsForSerialize(instance, isForSubmit) {
    if (typeof filter_result_url !== 'undefined') {
      var serializeList = [
        serializeFilter(instance.find('.filter__column--category-list,.filter__row--category-list')),
        serializeFilter(instance.find('.filter__column--opacity-list,.filter__row--opacity-list')),
        serializeFilter(instance.find('.filter__column--color-list,.filter__row--color-list'))
      ];

      if (isForSubmit && !serializeList[0].length && !serializeList[1].length && !serializeList[2].length) {
        serializeList = [
          serializeFilter(instance.find('.filter__column--category-list,.filter__row--category-list'), true),
          serializeFilter(instance.find('.filter__column--opacity-list,.filter__row--opacity-list'), true),
          serializeFilter(instance.find('.filter__column--color-list,.filter__row--color-list'), true)
        ];
      }

      var queryString = [
        createStringList('category_list=', serializeList[0]),
        createStringList('opacity_list=', serializeList[1]),
        createStringList('color_list=', serializeList[2])
      ];

      return [
        filter_result_url,
        queryString.join('&')
      ];
    }
  }

  function getElementsForSerializeSubmit(instance) {
    if (typeof filter_result_url !== 'undefined') {
      var serializeList = [
        serializeFilterSubmit(instance.find('.filter__column--category-list,.filter__row--category-list')),
        serializeFilterSubmit(instance.find('.filter__column--opacity-list,.filter__row--opacity-list')),
        serializeFilterSubmit(instance.find('.filter__column--color-list,.filter__row--color-list'))
      ];

      var queryString = [
        createStringList('category_list=', serializeList[0]),
        createStringList('opacity_list=', serializeList[1]),
        createStringList('color_list=', serializeList[2])
      ];

      return [
        filter_result_url,
        queryString.join('&')
      ];
    }
  }

  function serializeFilter(instance, isEmpty) {
    var serializeList = instance.serializeArray();

    var all_checkbox = instance.find('input[type="checkbox"]');
    var only_checked = instance.find('input[type="checkbox"]:checked');
    var inputList = (isEmpty) ? all_checkbox : only_checked;

    return serializeList.concat(
      inputList.map(function () {
        return {
          name: this.getAttribute('data-slug'),
          value: this.value
        };
      }).get()
    );
  }

  function serializeFilterSubmit(instance) {
    var serializeList = instance.serializeArray();

    var all_checkbox = instance.find('input[type="checkbox"]');
    var only_checked = instance.find('input[type="checkbox"]:checked');
    var inputList = (only_checked.length) ? only_checked : all_checkbox;

    return serializeList.concat(
      inputList.map(function () {
        return {
          name: this.getAttribute('data-slug'),
          value: this.value
        };
      }).get()
    );
  }

  function createStringList(queryString, serializeList) {
    for (var i = 0, len = serializeList.length; i < len; i++) {
      queryString += serializeList[i].value;
      if (i !== (len - 1)) queryString += ',';
    }

    return queryString;
  }

  function create_plural_string(number, text) {
    var cases = [2, 0, 1, 1, 1, 2];

    var formatted_number = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    var plural_string = text[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];

    return '<span class="products-amount__number">' + formatted_number + '</span> ' + plural_string;
  }

  var requestFilter = (function () {
    var time_window = 750; // time in ms
    var timeout;

    var send = function (state, instance, callback) {
      instance.find('.products-amount').addClass('spin-that-shit');
      jQuery.post(goods_filter.ajax_url + "?ts=" + new Date().getMilliseconds(), {
        dataType: 'text',
        action: 'get_filter_count',
        security: goods_filter.security,
        query_string: state,
        cache: false
      }, callback);
    };

    return function () {
      var context = this;
      var args = arguments;
      clearTimeout(timeout);
      timeout = setTimeout(function () {
        send.apply(context, args);
      }, time_window);
    };
  }());

  function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
  }
}());
