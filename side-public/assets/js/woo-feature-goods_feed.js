(function () {
  'use strict';

  document.addEventListener('DOMContentLoaded', function () {
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 'auto',
      prevButton: '.arrow-navigation__button--prev',
      nextButton: '.arrow-navigation__button--next',
      buttonDisabledClass: 'arrow-navigation__button--disabled'
    });

    var instanceList = document.querySelectorAll('.woo-feature-goods.category-feed');

    if (instanceList.length !== 0) {
      for (var i = 0, len = instanceList.length; i < len; i++) {
        var instance = jQuery(instanceList[i]);

        updateFakeHeadingHeight(instance);
        updateCustomSlideHeight(instance);
        toggleQuadraLayout(instance);
        updateSwiperLayout(instance);
      }

      resizeEventListener(function () {
        for (var i = 0, len = instanceList.length; i < len; i++) {
          var instance = jQuery(instanceList[i]);

          updateFakeHeadingHeight(instance);
          updateCustomSlideHeight(instance);
          toggleQuadraLayout(instance);
          updateSwiperLayout(instance);
        }
      });
    }
  });

  function resizeEventListener(callback) {
    var timeout = false;
    var delay = 250;

    window.addEventListener('resize', function () {
      clearTimeout(timeout);
      timeout = setTimeout(callback, delay);
    });
  }

  function updateFakeHeadingHeight(instance) {
    var headline = instance.find('.headline');
    var backdrop = instance.find('.subtitle-backdrop');

    backdrop.height(headline.outerHeight());
  }

  function updateCustomSlideHeight(instance) {
    setTimeout(function () {
      var product = instance.find('.wp-post-image').first();
      var custom = instance.find('.cover-image');

      custom.height(product.outerHeight());

      for (var i = 0, len = custom.length; i < len; i++) {
        if (custom[i].classList.contains('is-bordered-slide')) {
          var value = product.outerHeight() - 4;
          custom[i].setAttribute('style', 'height: ' + value + 'px;');
        }
      }
    }, 250);
  }

  function toggleQuadraLayout(instance) {
    var featured = instance.find('.featured-slide');
    var product = instance.find('.product-slide');

    if (instance.outerWidth() > 1000) {
      featured.addClass('featured-slide--quadra-layout');
      product.addClass('product-slide--quadra-layout');
    } else {
      featured.removeClass('featured-slide--quadra-layout');
      product.removeClass('product-slide--quadra-layout');
    }
  }

  function updateSwiperLayout(instance) {
    setTimeout(function () {
      instance.find('.swiper-container').each(function () {
        this.swiper.update(true);
      });
    }, 250);
  }
}());
