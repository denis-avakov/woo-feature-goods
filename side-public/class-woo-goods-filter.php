<?php

/**
 * The public-facing functionality of the goods filter shortcode.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-public
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Goods_Filter_Shortcode {
  private $atts;

  public function __construct($atts) {
    $this->atts = $atts;
  }

  public function start() {
    $atts = shortcode_atts(array(
      'headline' => '',
      'category_ids' => ''
    ), $this->atts);

    $category_ids_array = $this->convert_string_to_array($atts['category_ids']);

    $mobile_category_list = $this->create_category_list($category_ids_array, true);
    $mobile_attribute_color_list = $this->create_attribute_color_list(true);
    $mobile_attribute_opacity_list = $this->create_attribute_opacity_list(true);

    $category_list = $this->create_category_list($category_ids_array);
    $attribute_color_list = $this->create_attribute_color_list();
    $attribute_opacity_list = $this->create_attribute_opacity_list();

    $attribute_count = $this->create_attribute_initial_count($atts['category_ids']);
    $mobile_attribute_count = $this->create_attribute_initial_count($atts['category_ids'], true);

    $filter_result_link = $this->get_filter_result_link();
    $preloader_text = $this->get_filter_preloader_text();

    include (plugin_dir_path(__FILE__) . 'templates/goods-filter/main.php');
  }

  private function convert_string_to_array($string) {
    $verified_string = preg_replace('/\.$/', '', $string);
    return explode(', ', $verified_string);
  }

  private function create_category_list($category_ids_array, $is_mobile = false) {
    $svg_icon = $this->create_svg_icon();
    $html_category_list = '';

    foreach ($category_ids_array as $category_id) {
      $cat_term = get_term($category_id, 'product_cat');

      if ($is_mobile) $html_category_list .= $this->create_filter_mobile_item($cat_term);
      else $html_category_list .= $this->create_filter_desktop_item($cat_term, $svg_icon);
    }

    return $html_category_list;
  }

  private function create_attribute_color_list($is_mobile = false) {
    $attribute_list = get_terms(array(
      'taxonomy' => 'pa_color',
      'hide_empty' => true
    ));

    $html_attribute_list = '';

    if (!empty($attribute_list) && !is_wp_error($attribute_list)) {
      foreach ($attribute_list as $attr_term) {
        if ($is_mobile) $html_attribute_list .= $this->create_filter_mobile_item($attr_term);
        else $html_attribute_list .= $this->create_filter_desktop_item($attr_term);
      }
    }

    return $html_attribute_list;
  }

  private function create_attribute_opacity_list($is_mobile = false) {
    $attribute_list = get_terms(array(
      'taxonomy' => 'pa_opasity',
      'hide_empty' => true
    ));

    // $svg_icon = $this->create_svg_icon();
    $html_attribute_list = '';

    if (!empty($attribute_list) && !is_wp_error($attribute_list)) {
      foreach ($attribute_list as $attr_term) {
        if ($is_mobile) $html_attribute_list .= $this->create_filter_mobile_item($attr_term);
        else $html_attribute_list .= $this->create_filter_desktop_item($attr_term);
      }
    }

    return $html_attribute_list;
  }

  private function create_svg_icon() {
    $svg_markup = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 49"><style>.path0 { fill: none; stroke: #575756; stroke-miterlimit: 10; } .path1 { fill: #ffffff; stroke: #42145f; stroke-miterlimit: 10; }</style><path class="path0" d="M30.9 3v2.1m0 10.5v2.2m0 10.4v2.2"/><path class="path1" d="M11.2 22.9L48.8 9.1l-9.4-6.8L1.1 15.2l10.1 7.7zm0 12.6l37.6-13.7-9.4-6.9-38.3 13 10.1 7.6zm0 12.6l37.6-13.7-9.4-6.9-38.3 13 10.1 7.6z"/><path class="path0" d="M41 7.3v32.1m-10-10s5.6 1.9 9.9 7.1M30.7 17s6 2.1 10.3 7.3M30.7 4.1s6 2.2 10.4 7.5"/></svg>';

    $titan = TitanFramework::getInstance('woo-feature-goods');
    $option_itemIcon = $titan->getOption('filter-item-icon');

    if ($option_itemIcon) {
      $result = sprintf('<img src="%s" width="24" height="24" />', wp_get_attachment_url($option_itemIcon));
    } else {
      $result = sprintf('<div class="svg-icon" style="display: none;">%s</div>', $svg_markup);
    }

    return $result;
  }

  private function create_filter_mobile_item($term) {
    $input = sprintf(
      '<input id="%s" type="checkbox" value="%s" data-slug="%s" />',
      ('filter-category-list__' . $term->slug), $term->term_id, $term->slug
    );

    return sprintf(
      '<label class="control control--checkbox %s">%s</label>',
      ('control--' . $term->slug), ($input . $term->name)
    );
  }

  private function create_filter_desktop_item($term, $svg_icon = null) {
    $input = sprintf(
      '<input id="%s" type="checkbox" value="%s" data-slug="%s" />',
      ('filter-category-list__' . $term->slug), $term->term_id, $term->slug
    );

    $indicator = sprintf('<div class="control__indicator">%s</div>', $svg_icon);
    $text = sprintf('<div class="control__text">%s</div>', $term->name);

    $label = sprintf(
      '<label class="control control--checkbox %s" title="%s">%s</label>',
      ('control--' . $term->slug), $term->name, ($input . $indicator . $text)
    );

    return sprintf('<div class="filter__item-group control-group" style="display: none;">%s</div>', $label);
  }

  private function create_attribute_initial_count($category_list, $is_mobile = false) {
    /*

    $total_products = $this->make_filter_query_search(array(
      'taxonomy' => 'product_cat',
      'field'    => 'id',
      'terms'    => explode(',', $category_list)
    ));

    return $this->create_plural_string(
      count($total_products),

      // plural form for 1, 2 and 5
      ($is_mobile) ? array('товар', 'товара', 'товаров') : array('товар выбран', 'товара выбрано', 'товаров выбрано')
    );

    */

    return sprintf(
      '<div class="products-amount">%s <span class="products-amount__number">%s</span> %s</div>',
      'Более', '3 000', 'товаров'
    );
  }

  private function make_filter_query_search($category_list) {
    return get_posts(array(
      'numberposts' => -1,
      'post_type'   => 'product',
      'post_status' => 'publish',
      'tax_query'   => array($category_list),
      'fields'      => 'ids' // only get post IDs
    ));
  }

  private function create_plural_string($number, $text) {
    $separate_number = number_format($number, 0, ',', ' ');
    $cases = array(2, 0, 1, 1, 1, 2);

    return sprintf(
      '<div class="products-amount"><span class="products-amount__number">%s</span> %s</div>',
      $separate_number, $text[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]]
    );
  }

  private function get_filter_result_link() {
    $titan = TitanFramework::getInstance('woo-feature-goods');
    $result_id = $titan->getOption('filter-settings-result-page-id');
    return get_page_link($result_id);
  }

  private function get_filter_preloader_text() {
    $titan = TitanFramework::getInstance('woo-feature-goods');
    return $titan->getOption('filter-preloader-text');
  }
}
