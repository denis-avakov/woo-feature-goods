<?php

/**
 * This file is used to markup the public-facing aspects of the shortcode.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-public/templates/category-feed
 * @author            Avakov Denis <den3er@gmail.com>
 */

?>

<div class="swiper-slide product-slide product-slide--last-slide product-slide--quadra-layout">
  <div class="cover-image <?php echo $cover_image_class_modifier; ?>" <?php echo $cover_inline_style; ?>>
    <a class="cover-image__url" href="<?php echo $atts['category_link']; ?>">

      <?php if (!$has_cover_image): ?>

        <?php
        $titan = TitanFramework::getInstance('woo-feature-goods');
        $option_icon = $titan->getOption('feed-last-slide-icon');
        ?>

        <?php if ($option_icon): ?>

          <div class="svg-icon">
            <img src="<?php echo wp_get_attachment_url($option_icon); ?>" />
          </div>

        <?php else: ?>

          <div class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="8 -10 50 54">
              <style>
                .path {
                  fill: #ffffff;
                  stroke: #333333;
                  stroke-linejoin: round;
                  stroke-miterlimit: 10;
                  stroke-width: 1.0582;
                }
              </style>

              <path class="path" d="M8.4 36.7l10.3 6.4 38.9-12.7-9.2-6.1zm0-34l10.3 6.5L57.6-3.6l-9.2-6zm0 11.2l10.3 6.5L57.6 7.6l-9.2-6zm0 11l10.2 6.6 39-12.4-9.1-6.1z" />
            </svg>
          </div>

          <span class="product-slide__title"><?php _e('Все товары', 'woo-feature-goods'); ?></span>

        <?php endif; ?>
      <?php endif; ?>

    </a>
  </div>

  <div class="product-slide__row">
    <?php echo $atts['category_button']; ?>
    <?php echo $atts['callback_button']; ?>
  </div>
</div>
