<?php

/**
 * This file is used to markup the public-facing aspects of the shortcode.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-public/templates/category-feed
 * @author            Avakov Denis <den3er@gmail.com>
 */

?>

<div class="swiper-slide featured-slide featured-slide--quadra-layout">
  <div class="cover-image">
    <a href="<?php echo $atts['category_link']; ?>">
      <div class="cover-image__inline-background" <?php echo $cover_inline_style; ?>></div>
      <div class="swap-hint"></div>

      <?php if (!empty($atts['desc_start_price'])): ?>
        <span class="featured-slide__start-price start-price">
          &#8381; <?php echo $atts['desc_start_price']; ?>
        </span>
      <?php endif; ?>
    </a>
  </div>

  <div class="featured-slide__row">
    <p><?php echo $atts['desc_text']; ?></p>
  </div>

  <?php if (!empty($atts['category_button']) || !empty($atts['callback_button'])): ?>
    <div class="featured-slide__row">
      <?php echo $atts['category_button']; ?>
      <?php echo $atts['callback_button']; ?>
    </div>
  <?php endif; ?>
</div>
