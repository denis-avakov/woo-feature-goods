<?php

/**
 * This file is used to markup the public-facing aspects of the shortcode.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-public/templates/category-feed
 * @author            Avakov Denis <den3er@gmail.com>
 */

?>

<section class="woo-feature-goods category-feed">
  <div class="subtitle-backdrop"></div>

  <div class="swiper-container">
    <div class="headline headline--arrow-navigation">
      <h5 class="headline__title"><?php echo $atts['headline']; ?></h5>

      <div class="arrow-navigation">
        <div class="arrow-navigation__button arrow-navigation__button--prev"></div>
        <div class="arrow-navigation__button arrow-navigation__button--next"></div>
      </div>
    </div>

    <div class="swiper-wrapper">
      <?php echo $part_first_slide; ?>

      <?php foreach ($products_ids_array as $product_id): ?>
        <div class="swiper-slide product-slide product-slide--quadra-layout">
          <?php echo do_shortcode('[product id="' . $product_id . '"]'); ?>
        </div>
      <?php endforeach; ?>

      <?php echo $part_last_slide; ?>
    </div>
  </div>
</section>
