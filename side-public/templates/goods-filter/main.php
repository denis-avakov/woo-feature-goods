<?php

/**
 * This file is used to markup the public-facing aspects of the shortcode.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-public/templates/goods-filter
 * @author            Avakov Denis <den3er@gmail.com>
 */

?>

<section class="woo-feature-goods goods-filter">
  <a class="button-normal button-secondary button--open-mobile-layout" href="#id7077">
    <span class="button__text">
      <?php _e('Быстрый подбор жалюзи и штор', 'woo-feature-goods'); ?>
    </span>
  </a>

  <div id="id7077" class="mobile-layout" style="display: none;">
    <div class="preloader" style="display: none;">
      <div class="preloader__wrapper">
        <div class="preloader__indicator"></div>
        <?php echo $preloader_text; ?>
      </div>
    </div>

    <form class="filter">
      <div class="filter__column filter__column--selector-list">
        <div class="filter__row filter__row--opacity-list">
          <h4><?php _e('Светопрозрачность', 'woo-feature-goods'); ?></h4>
          <div class="selector-list"><?php echo $mobile_attribute_opacity_list; ?></div>
        </div>

        <div class="filter__row filter__row--color-list">
          <h4><?php _e('Цвет', 'woo-feature-goods'); ?></h4>
          <div class="selector-list"><?php echo $mobile_attribute_color_list; ?></div>
        </div>

        <div class="filter__row filter__row--category-list">
          <h4><?php _e('Виды товаров', 'woo-feature-goods'); ?></h4>
          <div class="selector-list"><?php echo $mobile_category_list; ?></div>
        </div>
      </div>

      <div class="filter__column filter__column--control-list">
        <a class="button-normal button--close-mobile-layout" href="#id7077"></a>

        <div class="submit-section">
          <div class="button__row">
            <?php echo $mobile_attribute_count; ?>
          </div>

          <div class="button__row">
            <a id="submit" class="button-normal button-primary button--mobile-submit" href="#">
              <span class="button__text"><?php _e('Показать выбранные', 'woo-feature-goods'); ?></span>
            </a>
          </div>
        </div>

        <a class="button-normal button--mobile-reset" href="#">
          <span class="button__text"><?php _e('Сбросить фильтр', 'woo-feature-goods'); ?></span>
        </a>
      </div>
    </form>
  </div>

  <div class="desktop-layout">
    <div class="headline">
      <a class="headline__link" href="#">
        <h5 class="headline__title">
          <?php echo $atts['headline']; ?>
        </h5>
      </a>
    </div>

    <form class="filter">
      <div class="preloader" style="display: none;">
        <div class="preloader__wrapper">
          <div class="preloader__indicator"></div>
          <?php echo $preloader_text; ?>
        </div>
      </div>

      <div class="filter__row">
        <div class="filter__column filter__column--category-list"><?php echo $category_list; ?></div>
        <div class="filter__column filter__column--color-list"><?php echo $attribute_color_list; ?></div>
        <div class="filter__column filter__column--opacity-list"><?php echo $attribute_opacity_list; ?></div>
      </div>

      <div class="filter__row">
        <a class="button-normal button--close-filter" href="#">
          <?php _e('Закрыть окно', 'woo-feature-goods'); ?>
        </a>

        <div class="filter__footer">
          <?php echo $attribute_count; ?>

          <a id="submit" class="button-normal button-primary" href="<?php echo $filter_result_link; ?>">
            <?php _e('Показать товары', 'woo-feature-goods'); ?>
          </a>
        </div>
      </div>
    </form>
  </div>

  <script type="text/javascript">
    <?php echo sprintf('var filter_result_url = "%s";', $filter_result_link); ?>
  </script>
</section>
