<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @package           Woo_Feature_Goods
 * @subpackage        Woo_Feature_Goods/side-public
 * @author            Avakov Denis <den3er@gmail.com>
 */

class Woo_Feature_Goods_Public {
  private $plugin_name;
  private $version;

  public function __construct($plugin_name, $version) {
    $this->plugin_name = $plugin_name;
    $this->version = $version;
  }


  // - [shortcode] Category Feed ----------------------------------------------

  public function register_shortcode_category_feed($atts) {
    require_once plugin_dir_path(dirname(__FILE__)) . 'side-public/class-woo-category-feed.php';

    $category_feed = new Woo_Category_Feed_Shortcode($atts);
    $category_feed -> start();

    $this->register_shortcode_category_feed_styles();
    $this->register_shortcode_category_feed_scripts();
  }

  private function register_shortcode_category_feed_styles() {
    $path = plugin_dir_url(__FILE__) . 'assets/css/woo-feature-goods_feed.css';
    wp_enqueue_style($this->plugin_name . '-category-feed', $path, array('dashicons'), $this->version, 'all');
  }

  private function register_shortcode_category_feed_scripts() {
    $path = plugin_dir_url(__FILE__) . 'assets/js/woo-feature-goods_feed.js';
    wp_enqueue_script($this->plugin_name . '-category-feed', $path, array('jquery'), $this->version, false);

    $path_swiper = 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js';
    wp_enqueue_script($this->plugin_name . '-vendor-swiper', $path_swiper, array(), $this->version, 'all');
  }


  // - [shortcode] Goods Filter -----------------------------------------------

  public function register_shortcode_goods_filter($atts) {
    // $this->nocache();

    require_once plugin_dir_path(dirname(__FILE__)) . 'side-public/class-woo-goods-filter.php';

    $goods_filter = new Woo_Goods_Filter_Shortcode($atts);
    $goods_filter -> start();

    $this->register_shortcode_goods_filter_styles();
    $this->register_shortcode_goods_filter_scripts();
  }

  private function register_shortcode_goods_filter_styles() {
    $path = plugin_dir_url(__FILE__) . 'assets/css/woo-feature-goods_filter.css';
    wp_enqueue_style($this->plugin_name . '-goods-filter', $path, array(), $this->version, 'all');

    $path_font_awesome = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css';
    wp_enqueue_style($this->plugin_name . '-goods-filter-font', $path_font_awesome, array(), $this->version, 'all');
  }

  private function register_shortcode_goods_filter_scripts() {
    $name = $this->plugin_name . '-goods-filter';
    $path = plugin_dir_url(__FILE__) . 'assets/js/woo-feature-goods_filter.js';
    wp_enqueue_script($name, $path, array('jquery'), $this->version, false);

    wp_localize_script($name, 'goods_filter', array(
      'ajax_url' => admin_url('admin-ajax.php'),
      'security' => wp_create_nonce('count_filtered_goods')
    ));
  }

  public function ajax_filter_goods_counter() {
    check_ajax_referer('count_filtered_goods', 'security');

    if (!empty($_POST['query_string'])) {
      parse_str($_POST['query_string'], $query_string);

      $search_result = $this->get_filter_products(array(
        'category_list' => !empty($query_string['category_list']) ? $query_string['category_list'] : null,
        'opacity_list' => !empty($query_string['opacity_list']) ? $query_string['opacity_list'] : null,
        'color_list' => !empty($query_string['color_list']) ? $query_string['color_list'] : null
      ));

      echo count($search_result);
    }

    die();
  }


  // - [shortcode] Filter Result ----------------------------------------------

  public function register_shortcode_filter_result() {
    // $this->nocache();

    // if (!defined('DONOTCACHEPAGE')) {
    //   define('DONOTCACHEPAGE', true);
    // }

    $this->register_shortcode_filter_result_styles();
    $this->register_shortcode_filter_result_scripts();

    parse_str($_SERVER['QUERY_STRING'], $query_string);

    if (!empty($query_string['category_list'])) {
      $category_list_array = explode(',', $query_string['category_list']);
      $opacity_list = !empty($query_string['opacity_list']) ? $query_string['opacity_list'] : null;
      $color_list = !empty($query_string['color_list']) ? $query_string['color_list'] : null;

      foreach ($category_list_array as $category_id) {
        $this->create_filter_result_category_feed(array(
          'category_id' => $category_id,
          'opacity_list' => $opacity_list,
          'color_list' => $color_list
        ));
      }
    } else {
      $titan = TitanFramework::getInstance('woo-feature-goods');
      echo $titan->getOption('filter-settings-result-empty');
    }
  }

  private function register_shortcode_filter_result_styles() {
    $path = plugin_dir_url(__FILE__) . 'assets/css/woo-feature-goods_result.css';
    wp_enqueue_style($this->plugin_name . '-filter-result', $path, array(), $this->version, 'all');
  }

  private function register_shortcode_filter_result_scripts() {
    $name = $this->plugin_name . '-filter-result';
    $path = plugin_dir_url(__FILE__) . 'assets/js/woo-feature-goods_result.js';
    wp_enqueue_script($name, $path, array('jquery'), $this->version, false);

    wp_localize_script($name, 'filter_result_pagination', array(
      'ajax_url' => admin_url('admin-ajax.php'),
      'security' => wp_create_nonce('create_new_pagination_page')
    ));
  }

  private function create_filter_result_category_feed($atts) {
    $search_result_atts = array(
      'category_list' => $atts['category_id'],
      'opacity_list' => $atts['opacity_list'],
      'color_list' => $atts['color_list']
    );

    $search_result = $this->get_filter_products($search_result_atts, 4);

    if (!empty($search_result)) {
      $category_term = get_term_by('id', $atts['category_id'], 'product_cat');
      $products_ids = implode(', ', $search_result);

      $html_title = sprintf(
        '<h3 class="%s" data-category_id="%s"><div>%s</div></h3>',
        'woocommerce-products-header__title page-title',
        $atts['category_id'], $category_term->name
      );

      $html_products = do_shortcode(sprintf('[products ids="%s"]', $products_ids));
      $html_button = '<a id="show-more" class="button-normal button-secondary" href="#">Показать больше</a>';

      echo sprintf('<div class="filter-result-section">%s</div>', $html_title . $html_products . $html_button);
    }
  }

  public function ajax_filter_result_pagination() {
    check_ajax_referer('create_new_pagination_page', 'security');

    if (!empty($_POST['query_string'])) {
      parse_str($_POST['query_string'], $query_string);
      if (!empty($query_string['category_list']) && !empty($query_string['exclude_product_ids'])) {

        $search_query_atts = array(
          'category_list' => $query_string['category_list'],
          'opacity_list' => $query_string['opacity_list'],
          'color_list' => $query_string['color_list']
        );

        $search_query_exclude = explode(',', $query_string['exclude_product_ids']);

        $search_result = $this->get_filter_products($search_query_atts, 12, $search_query_exclude);

        if (count($search_result)) {
          $products_ids = implode(', ', $search_result);
          echo do_shortcode(sprintf('[products ids="%s"]', $products_ids));
        } else {
          echo 'silence is golden';
        }
      }
    }

    die();
  }


  // - [utility] Products Filter ----------------------------------------------

  private function get_filter_products($atts = null, $products_per_feed = -1, $exclude = null) {
    $search_query = array();

    if ($atts['category_list']) {
      $search_query[] = array(
        'taxonomy' => 'product_cat',
        'field'    => 'id',
        'terms'    => explode(',', $atts['category_list'])
      );
    }

    if ($atts['color_list']) {
      $search_query[] = array(
        'taxonomy' => 'pa_color',
        'field'    => 'id',
        'terms'    => explode(',', $atts['color_list'])
      );
    }

    if ($atts['opacity_list']) {
      $search_query[] = array(
        'taxonomy' => 'pa_opasity',
        'field'    => 'id',
        'terms'    => explode(',', $atts['opacity_list'])
      );
    }

    return get_posts(array(
      'numberposts' => $products_per_feed,
      'exclude'     => $exclude,
      'post_type'   => 'product',
      'tax_query'   => $search_query,
      'fields'      => 'ids' // only get post IDs
    ));
  }

  // private function nocache() {
  //   if (!defined('DONOTCACHEPAGE')) {
  //     define('DONOTCACHEPAGE', true);
  //   }

  //   if (defined('DONOTCACHEOBJECT')) {
  //     define('DONOTCACHEOBJECT', true);
  //   }

  //   if (!defined('DONOTCACHEDB')) {
  //     define('DONOTCACHEDB', true);
  //   }
  // }
}
